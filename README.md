# Gitlab-CI Runner Vagrant+VirtualBox

A repository to add personal ***runners*** to your Gitlab-CI project.

This is a *Docker runner excutor*.

***pay attention 1***: If yours gitlab servers have self-signed certificates or theirs certificates are not reconized by *operating systems*, *browsers* and other o.s. embed tools, you need to take the `server_certificate.crt` to set *tls* message transfers.

## Tecnologies

- [Gitlab-Runner](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner)
- [VirtualBox](http://virtualbox.org/)
- [Vagrant](http://vagrantup.com/)
- [Docker](http://docker.com)

## How to

1. Prepare environment to run a *Gitlab-CI Runner VM*

  - `URL_GIT_SERVER` = your gitlab-ci server name (default: www-hscm)
  - `TOKEN` = the token of your project runner (default: none/7HqYCfec9H2xPj_oeh9G)

  Export these variables before starting VMs, e.g:

  ```shell
  export URL_GIT_SERVER='<www-scm>'
  export TOKEN='<7HqYCfec9H2xPj_oeh9G>'
  ```

1. Take your gitlab CA-Certificate

  If yours gitlab servers have self-signed certificates or theirs certificates are not reconized by *operating systems*, *browsers* and others o.s. embed tools, you need to take the `URL_GIT_SERVER.crt` and copy it to `./setup/ca-certs`.

1. Set some ariables to register on your project:

  According to your gitlab environment and your `.gitlab-ci.yml` it can be necessary to set some project variables to be able before start running builds.

  - `PRIVATE_TOKEN`:	For SSH fixtures, set your gitlab login token (optional) (e.g: `https://<gitlab.com>/profile/account`)
  - `KNOWN_HOSTS`: 	SSH known hosts for SSH fixtures, set *Key fingerprints* of all gitlab servers (optional)  
    e.g: `example.com,192.168.33.105 ecdsa-sha2-nistp256 FOOfingerprintKeyVjZHNhLXNoYTItbmlzdHAyNTYAAAAIbml+74UOzS2uXKqJM1Us+zt3Thf=`
  - `http_proxy`:  `<http://proxy:port>` (optional)
  - `HTTP_PROXY`:  `<http://proxy:port>` (optional)
  - `https_proxy`: `<http://proxy:port>` (optional)
  - `HTTPS_PROXY`: `<http://proxy:port>` (optional)
  - `no_proxy`: 	`127.0.0.1,localhost,.prevnet` (optional)
  - `NO_PROXY`: 	`127.0.0.1,localhost,.prevnet` (optional)

1. Start and stopping ***Gitlab-CI Runners***

  You'll need to use *`vagrant`* tool, e.g.:

  - `vagrant up`: to create a *Gitlab-CI Runner VM*
  - `vagrant destroy -f`: to delete a *Gitlab-CI Runner VM*

  Depending your desktop/host settings, you can create more them one *vm runner* at once. Set the environment variable `NODES` with the amount of nodes you want. For the beginnig it start to the same project.

## *PROXY* settings

Create a partial ***`Vagranfile`*** on your own home directory, e.g:

- `/home/adriano.vieira/.vagrand.d/Vagranfile`

With the following content:

```ruby
Vagrant.configure("2") do |config|
  if Vagrant.has_plugin?("vagrant-proxyconf")
    config.proxy.http     = "http://<PROXY>:<PORT>/"
    config.proxy.https    = "http://<PROXY>:<PORT>/"
    config.proxy.no_proxy = "localhost,127.0.0.1,.prevnet,.hacklab"
  end
end
```
