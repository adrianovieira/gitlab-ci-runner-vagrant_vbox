# -*- mode: ruby -*-
# vi: set ft=ruby :
'''
/**
 * GitLab-CI Runners
 *
 * Vagrantfile of Runners for GitLab-CI
 * plugins:
 * - vagrant-proxyconf
 * - vagrant-triggers (opcional)
 *
 * `$ vagrant plugin install vagrant-triggers`
 *
 * @author Adriano Vieira <adriano.svieira at gmail.com>

   Copyright 2016 Adriano dos Santos Vieira

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 * @license @see LICENCE
 */
'''

# Número de nodes para runners (default: 1)
NODES = (ENV.key?('NODES') ? ENV['NODES'].to_i : 1)

# Projeto: https://www-hscm/adriano.vieira/gitlab-ci-test/runners
# https://www-hscm.prevnet/adriano.vieira/gitlab-ci-test/variables
# Variáveis a serem cadastradas no projeto:
#        http_proxy 	<http://proxy:port>
#        HTTP_PROXY 	<http://proxy:port>
#       https_proxy 	<http://proxy:port>
#       HTTPS_PROXY 	<http://proxy:port>
#       KNOWN_HOSTS 	www-git.prevnet,10.0.143.105 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBN8lYHuuIB1l8tApahtDpu2BM+74UOzS2uXKqJM1Us+zt3Thf+zT1icQL1D6y8hh4Xz6Z3U0a4NfJ4p1TGYfjEg=
#          no_proxy 	127.0.0.1,localhost,.prevnet
#          NO_PROXY 	127.0.0.1,localhost,.prevnet
#      PRIVATE_TOKEN 	<sua chave privada nos servidores git>
#
# Token do projeto (para registrar o runner)
#     7HqYCfec9H2xPj_oeh9G
URL_GIT_SERVER = (ENV.key?('URL_GIT_SERVER') ? ENV['URL_GIT_SERVER'] : 'www-hscm')
TOKEN = (ENV.key?('TOKEN') ? ENV['TOKEN'] : '7HqYCfec9H2xPj_oeh9G')

# daemon que irá executar os jobs especificados no Gitlab-CI
EXECUTOR = (ENV.key?('EXECUTOR') ? ENV['EXECUTOR'] : 'docker')
EXECUTOR_IMAGE = (ENV.key?('EXECUTOR_IMAGE') ? ENV['EXECUTOR_IMAGE'] : 'ruby:2.1')

Vagrant.configure("2") do |config|
  config.vm.box = "adrianovieira/centos7-docker1.12-GA"

  config.vm.provision "docker-setup-proxy", type: "shell",
          path: "scripts/docker-setup-proxy.sh"

  config.vm.provision "gitlab-install-addrepo", type: "shell",
          path: "scripts/gitlab-ci-multi-runner.script.rpm.sh"

  config.vm.provision "gitlab-install-runner_service", type: "shell",
          inline: "yum install -y gitlab-ci-multi-runner"

  config.vm.provision "ca-cert-add", type: "shell",
          inline: "cp /vagrant/setup/ca-certs/* /etc/pki/ca-trust/source/anchors/
                   update-ca-trust"

  config.vm.provision "gitlab-setup-runner-register", type: "shell",
          inline: "gitlab-ci-multi-runner register \
                           --non-interactive --url https://#{URL_GIT_SERVER}/ci --registration-token #{TOKEN} \
                           --executor docker \
                           --tls-ca-file /etc/pki/ca-trust/source/anchors/#{URL_GIT_SERVER}.crt \
                           --tag-list ci,docker,vbox,vagrant \
                           --docker-pull-policy if-not-present \
                           --docker-image ruby:2.1"

  config.vm.provider "virtualbox" do |virtualbox| # general Virtualbox.settings
    virtualbox.customize [ "modifyvm", :id, "--cpus", 2 ]
    virtualbox.customize [ "modifyvm", :id, "--memory", 2048 ]
    virtualbox.customize [ "modifyvm", :id, "--name", 'vm.prevnet' ]
    virtualbox.customize [ "modifyvm", :id, "--groups", "/gitlab-runner" ]
  end # end Virtualbox.settings

  (1..(NODES)).each do |node_id|
    config.vm.define "runner-node-#{node_id}" do |node|  # define-VM
      node.vm.hostname = "runner-node-#{node_id}.prevnet"

      node.vm.provider "virtualbox" do |virtualbox| # Virtualbox.settings
        virtualbox.customize [ "modifyvm", :id, "--name", "runner-node-#{node_id}.prevnet" ]
      end # end Virtualbox.settings

      # opcional para remover runner do projeto no gitlab (só remove de um por enquanto)
      #if Vagrant.has_plugin?("vagrant-triggers")
      #  node.trigger.before :destroy do
      #    info "ungeristering gitlab-runner"
      #    run_remote  "gitlab-ci-multi-runner unregister --name runner-node-#{node_id}.prevnet"
      #  end
      #end

    end # end-of-define-VM
  end # end-of-define-VM-loop node_id

  config.vm.provision "docker-pull-base-images", type: "shell",
          inline: "docker pull registry.gitlab.com/osinfs/containerize:base-puppet_tests-alpine34
                   docker pull registry.gitlab.com/osinfs/containerize:base-puppet_tests-centos7
                   docker pull registry.gitlab.com/osinfs/containerize:base-puppet_tests-debian8"

end
