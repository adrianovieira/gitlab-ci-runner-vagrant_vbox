# Gitlab-CI Runner Vagrant+VirtualBox

## CONTRIBUTING

Contribute to THIS_PROJECT.

Thank you for your interest in contributing to THIS_PROJECT. This guide details how to contribute to THIS_PROJECT in a way that is efficient for everyone.

If you have read this contributing terms and want to know how the THIS_PROJECT operates please also help us to build and/or improve our contributing process.

### Contributor license agreement

By submitting code as an individual you agree to the **individual contributor license agreement**. By submitting code as an entity you agree to the **corporate contributor license agreement**.

### Individual contributor license agreement

I, `$yourname`, accept and agree to the following terms and conditions for my present and future Contributions submitted to THIS_PROJECT (or to the *`The Core Team`* behind it). Except for the license granted herein to THIS_PROJECT and recipients of software distributed by *`The Core Team`*, I reserve all right, title, and interest in and to my Contributions.

1.  Definitions.  
  - *`The Core Team`* see [TCT.md](TCT.md)  
	- "You" (or "Your") shall mean the copyright owner or legal entity authorized by the copyright owner that is making this Agreement with *`The Core Team`*. For legal entities, the entity making a Contribution and all other entities that control, are controlled by, or are under common control with that entity are considered to be a single Contributor. For the purposes of this definition, "control" means (i) the power, direct or indirect, to cause the direction or management of such entity, whether by contract or otherwise, or (ii) ownership of fifty percent (50%) or more of the outstanding shares, or (iii) beneficial ownership of such entity.  
	- "Contribution" shall mean any original work of authorship, including any modifications or additions to an existing work, that is intentionally submitted by You to APSS and to *`The Core Team`* for inclusion in, or documentation of, any of the products owned or managed by *`The Core Team`* (the "Work"). For the purposes of this definition, "submitted" means any form of electronic, verbal, or written communication sent to *`The Core Team`* or its representatives, including but not limited to communication on electronic mailing lists, source code control systems, and issue tracking systems that are managed by, or on behalf of, *`The Core Team`* for the purpose of discussing and improving the Work, but excluding communication that is conspicuously marked or otherwise designated in writing by You as "Not a Contribution."  

2.  Grant of Copyright License. Subject to the terms and conditions of this Agreement, You hereby grant to *`The Core Team`* and to recipients of software distributed by *`The Core Team`* a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable copyright license to reproduce, prepare derivative works of, publicly display, publicly perform, sublicense, and distribute Your Contributions and such derivative works.

3.  Grant of Patent License. Subject to the terms and conditions of this Agreement, You hereby grant to *`The Core Team`* and to recipients of software distributed by *`The Core Team`* a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable (except as stated in this section) patent license to make, have made, use, offer to sell, sell, import, and otherwise transfer the Work, where such license applies only to those patent claims licensable by You that are necessarily infringed by Your Contribution(s) alone or by combination of Your Contribution(s) with the Work to which such Contribution(s) was submitted. If any entity institutes patent litigation against You or any other entity (including a cross-claim or counterclaim in a lawsuit) alleging that your Contribution, or the Work to which you have contributed, constitutes direct or contributory patent infringement, then any patent licenses granted to that entity under this Agreement for that Contribution or Work shall terminate as of the date such litigation is filed.

4.  You represent that you are legally entitled to grant the above license. If your employer(s) has rights to intellectual property that you create that includes your Contributions, you represent that you have received permission to make Contributions on behalf of that employer, that your employer has waived such rights for your Contributions to *`The Core Team`*, or that your employer has executed a separate Corporate CLA with *`The Core Team`*.

5.  You represent that each of Your Contributions is Your original creation (see section 7 for submissions on behalf of others). You represent that Your Contribution submissions include complete details of any third-party license or other restriction (including, but not limited to, related patents and trademarks) of which you are personally aware and which are associated with any part of Your Contributions.

6.  You are not expected to provide support for Your Contributions, except to the extent You desire to provide support. You may provide support for free, for a fee, or not at all. Unless required by applicable law or agreed to in writing, You provide Your Contributions on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied, including, without limitation, any warranties or conditions of TITLE, NON- INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A PARTICULAR PURPOSE.

7.  Should You wish to submit work that is not Your original creation, You may submit it to *`The Core Team`* separately from any Contribution, identifying the complete details of its source and of any license or other restriction (including, but not limited to, related patents, trademarks, and license agreements) of which you are personally aware, and conspicuously marking the work as "Submitted on behalf of a third-party: [[]named here]".

8.  You agree to notify *`The Core Team`* of any facts or circumstances of which you become aware that would make these representations inaccurate in any respect.

This text is licensed under the [Creative Commons Attribution 3.0 License](https://creativecommons.org/licenses/by/3.0/) and the original source is the GitLab.org / GitLab Community Edition files.


### Corporate Contributor License Agreement

You, `$yourname`, accept and agree to the following terms and conditions for your present and future Contributions submitted to THIS_PROJECT (or to the *`The Core Team`* behind it). Except for the license granted herein to the *`The Core Team`* and recipients of software distributed by the *`The Core Team`*, You reserve all right, title, and interest in and to your Contributions.

1.  Definitions.  
  - *`The Core Team`* see [TCT.md](TCT.md)  
	- "You" (or "Your") shall mean the copyright owner or legal entity authorized by the copyright owner that is making this Agreement with *`The Core Team`*. For legal entities, the entity making a Contribution and all other entities that control, are controlled by, or are under common control with that entity are considered to be a single Contributor. For the purposes of this definition, "control" means (i) the power, direct or indirect, to cause the direction or management of such entity, whether by contract or otherwise, or (ii) ownership of fifty percent (50%) or more of the outstanding shares, or (iii) beneficial ownership of such entity.  
	- "Contribution" shall mean the code, documentation or other original works of authorship expressly identified in Schedule B, as well as any original work of authorship, including any modifications or additions to an existing work, that is intentionally submitted by You to *`The Core Team`* for inclusion in, or documentation of, any of the products owned or managed by *`The Core Team`* (the "Work"). For the purposes of this definition, "submitted" means any form of electronic, verbal, or written communication sent to *`The Core Team`* or its representatives, including but not limited to communication on electronic mailing lists, source code control systems, and issue tracking systems that are managed by, or on behalf of, *`The Core Team`* for the purpose of discussing and improving the Work, but excluding communication that is conspicuously marked or otherwise designated in writing by You as "Not a Contribution."  

2.  Grant of Copyright License. Subject to the terms and conditions of this Agreement, You hereby grant to *`The Core Team`* and to recipients of software distributed by *`The Core Team`* a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable copyright license to reproduce, prepare derivative works of, publicly display, publicly perform, sublicense, and distribute Your Contributions and such derivative works.

3.  Grant of Patent License. Subject to the terms and conditions of this Agreement, You hereby grant to *`The Core Team`* and to recipients of software distributed by *`The Core Team`* a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable (except as stated in this section) patent license to make, have made, use, offer to sell, sell, import, and otherwise transfer the Work, where such license applies only to those patent claims licensable by You that are necessarily infringed by Your Contribution(s) alone or by combination of Your Contribution(s) with the Work to which such Contribution(s) was submitted. If any entity institutes patent litigation against You or any other entity (including a cross-claim or counterclaim in a lawsuit) alleging that your Contribution, or the Work to which you have contributed, constitutes direct or contributory patent infringement, then any patent licenses granted to that entity under this Agreement for that Contribution or Work shall terminate as of the date such litigation is filed.

4.  You represent that You are legally entitled to grant the above license. You represent further that each employee of the Corporation designated on Schedule A below (or in a subsequent written modification to that Schedule) is authorized to submit Contributions on behalf of the Corporation.

5.  You represent that each of Your Contributions is Your original creation (see section 7 for submissions on behalf of others).

6.  You are not expected to provide support for Your Contributions, except to the extent You desire to provide support. You may provide support for free, for a fee, or not at all. Unless required by applicable law or agreed to in writing, You provide Your Contributions on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied, including, without limitation, any warranties or conditions of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A PARTICULAR PURPOSE.

7.  Should You wish to submit work that is not Your original creation, You may submit it to *`The Core Team`* separately from any Contribution, identifying the complete details of its source and of any license or other restriction (including, but not limited to, related patents, trademarks, and license agreements) of which you are personally aware, and conspicuously marking the work as "Submitted on behalf of a third-party: [named here]".

8.  It is your responsibility to notify *`The Core Team`* when any change is required to the list of designated employees authorized to submit Contributions on behalf of the Corporation, or to the Corporation's Point of Contact with *`The Core Team`*.


This text is licensed under the [Creative Commons Attribution 3.0 License](https://creativecommons.org/licenses/by/3.0/) and the original source is the GitLab.org / GitLab Community Edition files.
